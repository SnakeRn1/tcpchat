#ifndef SOCKET_THREAD_H
#define SOCKET_THREAD_H
#include <QThread>
#include <QTcpSocket>
#include <QTime>
#include <QFile>
#include <QFileInfo>
#include <QFileDialog>
#include <QList>
#include <QSettings>
#include <QDesktopServices>
#include <data_structure.h>

struct transfer_struct
{
    bool file_transmit = false;
    bool file_receive = false;
    qint64 wait_size = 0;
    qint64 full_file_size = 0;
    qint64 file_received = 0;
    QString name_transmitter;
    QFile file;
    QByteArray transmit_buffer;
    void startReceive(const QString& file_name, const qint64 file_size, const QString& name_from)
    {
        file_receive = true;
        QString temp_user_name(name_from);
        QString temp_file_name(file_name);
        QRegExp temp_win_syms("[:/*?<>|\\\\+%!\"]");// Внимание. Чтобы выделить косую черту "\" в RegExp надо занести четыре черты. Это экранирование.
        temp_user_name.remove(temp_win_syms);
        temp_file_name.remove(temp_win_syms);
        if(temp_user_name.isEmpty()) temp_user_name = "temp_user_name";
        if(temp_file_name.isEmpty()) temp_file_name = "temp_file_name";
        if(!QDir(temp_user_name).exists()) QDir().mkdir(temp_user_name);
        file.setFileName(temp_user_name + "/" + temp_file_name);
        file.open(QFile::WriteOnly);
        full_file_size = file_size;
        file_received = 0;
        name_transmitter = name_from;
    }
    void resetReceive()
    {
        file_receive = false;
        wait_size = 0;
        full_file_size = 0;
        file_received = 0;
        file.close();
        name_transmitter.clear();
    }
    void startTransmit()
    {
        file_transmit = true;
    }
    void resetTransmit()
    {
        file_transmit = false;
    }
};

class socket_thread : public QObject
{
    Q_OBJECT
public:
    socket_thread(QString hostname, QString username);
    ~socket_thread();
    bool isTransmit()
    {
        return wait.file_transmit;
    }

private slots:
    void slotReadyRead ();
    void slotError (QAbstractSocket::SocketError);
    void slotConnected ();
    void slotDisconnected();

public slots:
    void start();
    void stop();
    void send_message (QString, QString);
    void send_file (QString, QString);
    void reconnect (QString,QString);

signals:
    void finished();
    void started();
    void file_percent_transmitted (int);
    void file_percent_received (int);
    void received_message (QString,QString);
    void sended_message (QString);
    void info_list_changed (QList<QString>,QString);
    void play_sound (QUrl);

private:
    QTcpSocket *m_socket;
    QString server;
    QString host_name;
    QString user_name;
    void sendStructure(data_structure &data);
    char* data_data;
    transfer_struct wait;
};

#endif // SOCKET_THREAD_H
