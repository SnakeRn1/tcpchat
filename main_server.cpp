#include "mytcpserver.h"

int main(int argc, char *argv[])
{
#ifdef SERV_CONS
    setlocale(LC_ALL,"Russian");
    QCoreApplication a(argc, argv);
    MyServer server(6000);
#else
    QApplication a(argc, argv);
    MyServer server(6000);
    server.show();
#endif
    a.exec();
    qDebug()<<"exit server";
    exit (0);
}
