#ifndef MYTCPSERVER_H
#define MYTCPSERVER_H

#include "data_structure.h"
#include <QObject>
#include <QTcpServer>
#include <QTcpSocket>
#include <QList>
#include <QtNetwork>

#ifdef SERV_CONS
#include <QCoreApplication>
#else
//#include <QApplication>
//#include <QWidget>
//#include <QTextEdit>
#endif

struct data_transfer
{
    bool file_tranmitter = false;
    bool file_receiver = false;
    qint64 wait_size = 0;
    qint64 full_file_size = 0;
    qint64 transmitted_size = 0;
    QTcpSocket* receiver = nullptr;
    QByteArray waiting_messages;
    void reset_transmitter()
    {
        wait_size = 0;
        file_tranmitter = false;
        full_file_size = 0;
        transmitted_size = 0;
        receiver = nullptr;
    }
    void setAsReceiver ()
    {
        file_receiver = true;
    }
    void resetReceiver ()
    {
        file_receiver = false;
    }
};

#ifdef SERV_CONS
class MyServer : public QObject
#else
class MyServer : public QTextEdit
#endif

{
    Q_OBJECT

public:

    MyServer(int nPort);
    ~MyServer();

public slots:

    void slotNewConnection();
    void slotUserDisconnected();
    void slotReadClient ();
    void slotErrorClient (QAbstractSocket::SocketError);

private:

    QTcpServer* m_ptcpServer;
    QMap<QTcpSocket*, QString> all_users;
    QMap<QTcpSocket*, data_transfer> rx_wait_sizes;
    QMap<QTcpSocket*, QByteArray> waiting_messages;
    char* file_data_block;

    void debug(QString);
    void sendToAllClients(QTcpSocket*, data_structure& data);
    void sendClientsList(data_structure& data, const QList<QString> &info, QString name);
    void tryToWriteSocketData(QTcpSocket *user, const QByteArray &array);
};



#endif // MYTCPSERVER_H
