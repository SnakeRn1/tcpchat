QT += core network
QT -= gui
CONFIG += c++11 console
TEMPLATE = app
TARGET = ../hola_chat-dst/HolaChatServerConsole
DEFINES += QT_DEPRECATED_WARNINGS SERV_CONS
SOURCES += main_server.cpp mytcpserver.cpp data_structure.cpp
HEADERS += mytcpserver.h data_structure.h
VERSION = 1.1.3

win32 {
RC_ICONS += icons/serv_ico.ico
QMAKE_TARGET_COMPANY = "SnakeR Soft"
QMAKE_TARGET_PRODUCT = "HolaChatServer"
QMAKE_TARGET_DESCRIPTION = "HolaChatServer_x64"
QMAKE_TARGET_COPYRIGHT = "Copyright © 2022 Horuzhiy Ruslan"
}

qnx: target.path = /tmp/$${TARGET}/bin
else: unix:!android: target.path = /opt/$${TARGET}/bin
!isEmpty(target.path): INSTALLS += target
