#include "data_structure.h"

data_structure::data_structure(const QTime &time,
                               const QString &type,
                               const QString &name_from,
                               const QString &mess, const
                               QString &name_to,
                               qint64 file_only_size)
{
    _time = time;
    _type = type;
    _name_from = name_from;
    _mess = mess;
    _name_to = name_to;
    _file_size = file_only_size;
    _time_str = _time.toString("HH:mm");
}

data_structure::data_structure(QDataStream &stream)
{
    read_in(stream);
}

data_structure::~data_structure()
{
    _block.clear();
}

QByteArray &data_structure::getData()
{
    if (_block.isEmpty())
    {
        QDataStream out(&_block, QIODevice::WriteOnly);
        write_out(out);
        out.device()->seek(0);
        out << qint64(_block.size() - sizeof(qint64));
    }
    return _block;
}

void data_structure::read_in(QDataStream &stream)
{
    stream >> _time >> _type >> _name_from >> _mess >> _name_to >> _file_size;
    _time_str = _time.toString("HH:mm");
}

void data_structure::write_out(QDataStream &stream)
{
    stream << qint64(0) << _time << _type << _name_from << _mess << _name_to << _file_size;
}


QDataStream &operator << (QDataStream &stream, data_structure &data)
{
    data.write_out(stream);
    return stream;
}

QDataStream &operator >> (QDataStream &stream, data_structure &data)
{
    data.read_in(stream);
    return stream;
}
