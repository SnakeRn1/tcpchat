#ifndef DATA_STRUCTURE_H
#define DATA_STRUCTURE_H
#include <QString>
#include <QTime>
#include <QDataStream>

#define BLOCK_FILE_SIZE 1048576

class data_structure
{
private:
    QTime _time;
    QString _type;
    QString _name_from;
    QString _mess;
    QString _name_to;
    qint64 _file_size;
    QString _time_str;
    QByteArray _block;

public:
    data_structure(const QTime& time,
                   const QString& type,
                   const QString& name_from,
                   const QString& mess,
                   const QString& name_to = QString("all"),
                   qint64 file_only_size = 0);
    data_structure(QDataStream &stream);
    ~data_structure();

    QTime& getTime(){return _time;}
    QString& getType(){return _type;}
    QString& getNameFrom(){return _name_from;}
    QString& getNameTo(){return _name_to;}
    QString& getMess(){return _mess;}
    QString& getTimeQstr() {return _time_str;}
    qint64& getFileOnlySize() {return _file_size;}
    QByteArray& getData();

    void read_in(QDataStream& stream);
    void write_out(QDataStream& stream);

    friend QDataStream& operator << (QDataStream& stream, data_structure& data);
    friend QDataStream& operator >> (QDataStream& stream, data_structure& data);
};

#endif // DATA_STRUCTURE_H
