QT += core gui widgets network multimedia
CONFIG += c++11
TEMPLATE = app

DEFINES += QT_DEPRECATED_WARNINGS
SOURCES += main_chat.cpp widget.cpp data_structure.cpp socket_thread.cpp
HEADERS += widget.h data_structure.h socket_thread.h
VERSION = 1.1.3
FORMS += widget.ui
RESOURCES += res.qrc \
    sound.qrc

win32 {
TARGET = ../../hola_chat-dst/HolaChat
RC_ICONS += icons/chat_ico.ico
QMAKE_TARGET_COMPANY = "SnakeR Soft"
QMAKE_TARGET_PRODUCT = "HolaChat"
QMAKE_TARGET_DESCRIPTION = "HolaChat_x64"
QMAKE_TARGET_COPYRIGHT = "Copyright © 2022 Horuzhiy Ruslan"
}

unix {
TARGET = ../hola_chat-dst/HolaChat
QMAKE_LFLAGS += -no-pie
RC_ICONS += icons/chat_ico.ico
}

qnx: target.path = /tmp/$${TARGET}/bin
else: unix:!android: target.path = /opt/$${TARGET}/bin
!isEmpty(target.path): INSTALLS += target
