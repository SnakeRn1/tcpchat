#include "mytcpserver.h"


MyServer::MyServer(int nPort)
{
    file_data_block = new char[BLOCK_FILE_SIZE];
    m_ptcpServer = new QTcpServer(this);
    if (!m_ptcpServer->listen(QHostAddress::Any, nPort))
    {
        m_ptcpServer->close();
        return;
    }
    connect(m_ptcpServer, SIGNAL(newConnection()),this, SLOT(slotNewConnection()));
#ifndef SERV_CONS
    resize(640,480);
#endif
}

MyServer::~MyServer()
{
    delete [] file_data_block;
    foreach(auto key, all_users.keys())
    {
        key->close();
        key->deleteLater();
    }
    m_ptcpServer->close();
    m_ptcpServer->deleteLater();
}

void MyServer::slotNewConnection()
{
    auto temp = m_ptcpServer->nextPendingConnection();
    rx_wait_sizes.insert(temp,data_transfer());
    connect(temp, SIGNAL(disconnected()), this, SLOT(slotUserDisconnected()));
    connect(temp, SIGNAL(readyRead()),this, SLOT(slotReadClient()));
    connect(temp, SIGNAL(error(QAbstractSocket::SocketError)),this,SLOT(slotErrorClient(QAbstractSocket::SocketError)));
    data_structure data(QTime::currentTime(),"request","server","NAME_REQUEST");
    sendToAllClients(temp, data);
}

void MyServer::slotUserDisconnected()
{
    QTcpSocket* pClientSocket = (QTcpSocket*)sender();
    QString name = all_users.value(pClientSocket);
    all_users.remove(pClientSocket);
    rx_wait_sizes.remove(pClientSocket);
    pClientSocket->close();
    pClientSocket->deleteLater();

    data_structure data3(QTime::currentTime(), "info", "server","CLIENTS_LIST");
    sendClientsList(data3, all_users.values(),"-1: " + name);
    debug("user " + name + " disconnected");
}

void MyServer::slotReadClient()
{
    QTcpSocket* pClientSocket = (QTcpSocket*)sender();
    QDataStream in(pClientSocket);

    while(pClientSocket->bytesAvailable())
    {
        if (rx_wait_sizes.value(pClientSocket).wait_size == 0)
        {
            if (pClientSocket->bytesAvailable() < 8) return;
            else in >> rx_wait_sizes[pClientSocket].wait_size;
        }
        if (pClientSocket->bytesAvailable() < rx_wait_sizes.value(pClientSocket).wait_size) return;

        if (rx_wait_sizes.value(pClientSocket).file_tranmitter)
        {
            qint64 retransmit_size = rx_wait_sizes[pClientSocket].wait_size;
            int i = in.readRawData(file_data_block,retransmit_size);
            if (i != retransmit_size)
            {
                debug("error: read_socket_bytes_file");
                break;
            }
            QTcpSocket* receiver = rx_wait_sizes.value(pClientSocket).receiver;
            qint64 p = receiver->write(file_data_block, retransmit_size);
            receiver->waitForBytesWritten();
            if (p != retransmit_size)
            {
                debug("error: write_socket_bytes_file");
                break;
            }
            rx_wait_sizes[pClientSocket].transmitted_size += retransmit_size;
            qint64 now_waiting = rx_wait_sizes.value(pClientSocket).full_file_size - rx_wait_sizes.value(pClientSocket).transmitted_size;
            if(now_waiting <= 0)
            {
                QString receiver_name = all_users.value(receiver);
                QString sender_name = all_users.value(pClientSocket);
                debug("file resending from " + sender_name + " to " + receiver_name + " done!");
                rx_wait_sizes[receiver].resetReceiver();
                rx_wait_sizes[pClientSocket].reset_transmitter();
                if(rx_wait_sizes.value(receiver).waiting_messages.size()>0)
                {
                    receiver->write(rx_wait_sizes.value(receiver).waiting_messages);
                    rx_wait_sizes[receiver].waiting_messages.clear();
                }
            }
            else
            {
                if (now_waiting>=BLOCK_FILE_SIZE) rx_wait_sizes[pClientSocket].wait_size = BLOCK_FILE_SIZE;
                else rx_wait_sizes[pClientSocket].wait_size = now_waiting;
            }
        }
        else
        {
            rx_wait_sizes[pClientSocket].wait_size = 0;
            data_structure data (in);

            if (data.getType() == "request")
            {
                if(data.getMess() == "NAME_FEEDBACK")
                {
                    all_users.insert(pClientSocket,data.getNameFrom());
                    debug("new user " + data.getNameFrom() + " connected");
                    data_structure data3(QTime::currentTime(), "info", "server","CLIENTS_LIST");
                    sendClientsList(data3, all_users.values(),"+1: " + data.getNameFrom());
                }
            }
            if (data.getType() == "message")
            {
                sendToAllClients(pClientSocket, data);
            }
            if (data.getType() == "file")
            {
                QTcpSocket* rx_user = all_users.key(data.getNameTo(),nullptr);
                sendToAllClients(rx_user,data);

                rx_wait_sizes[rx_user].setAsReceiver();

                rx_wait_sizes[pClientSocket].receiver = rx_user;
                rx_wait_sizes[pClientSocket].file_tranmitter = true;
                rx_wait_sizes[pClientSocket].full_file_size = data.getFileOnlySize();
                rx_wait_sizes[pClientSocket].transmitted_size = 0;
                if(data.getFileOnlySize()>=BLOCK_FILE_SIZE) rx_wait_sizes[pClientSocket].wait_size = BLOCK_FILE_SIZE;
                else rx_wait_sizes[pClientSocket].wait_size = data.getFileOnlySize();
            }
            debug(data.getType() + " " + data.getTimeQstr() + " from " + data.getNameFrom() + " to " + data.getNameTo());
        }
    }
}

void MyServer::slotErrorClient(QAbstractSocket::SocketError)
{
    QTcpSocket* pClientSocket = (QTcpSocket*)sender();
    debug("socket "  +  all_users.value(pClientSocket)  +  " error: " + pClientSocket->errorString());
}

void MyServer::debug(QString str)
{
#ifdef SERV_CONS
    QTextStream cin(stdin);
    qDebug()<<str;
#else
    append(str);
#endif
}

void MyServer::sendToAllClients(QTcpSocket* user, data_structure& data)
{
    if (data.getType() == "request")
    {
        user->write(data.getData());
    }

    if (data.getType() == "message")
    {
        if (data.getNameTo() == "all")
        {
            for(auto key:all_users.keys())
            {
                if(key!=user) tryToWriteSocketData(key,data.getData());
            }
        }
        else
        {
            tryToWriteSocketData(all_users.key(data.getNameTo()),data.getData());
        }
    }

    if (data.getType() == "file")
    {
        tryToWriteSocketData(user,data.getData());
    }
}

void MyServer::sendClientsList(data_structure& data, const QList<QString>& info, QString name)
{
    QByteArray arrBlock;
    QDataStream out(&arrBlock, QIODevice::WriteOnly);

    out << data;
    out << name;
    out << info;

    out.device()->seek(0);
    out << qint64(arrBlock.size() - sizeof(qint64));

    if (data.getType() == "info")
    {
        for(auto key:all_users.keys())
        {
            tryToWriteSocketData(key,arrBlock);
        }
    }
}

void MyServer::tryToWriteSocketData(QTcpSocket* user, const QByteArray& array)
{
    if(rx_wait_sizes.value(user).file_receiver)
    {
        rx_wait_sizes[user].waiting_messages.append(array);
        debug("appending messages to waiting");
    }
    else user->write(array);
}


