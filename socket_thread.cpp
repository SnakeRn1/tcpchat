#include "socket_thread.h"

socket_thread::socket_thread(QString hostname, QString username)
    : host_name(hostname), user_name(username)
{
    data_data = new char[BLOCK_FILE_SIZE];
}

socket_thread::~socket_thread()
{
    delete [] data_data;
    wait.resetReceive();
    emit finished();
}

void socket_thread::start()
{
    m_socket = new QTcpSocket();
    m_socket->connectToHost(host_name,6000);

    connect(m_socket, SIGNAL(connected()),
            this, SLOT(slotConnected()),
            Qt::DirectConnection);
    connect(m_socket, SIGNAL(disconnected()),
            this, SLOT(slotDisconnected()),
            Qt::DirectConnection);
    connect(m_socket, SIGNAL(readyRead()),
            this, SLOT(slotReadyRead()),
            Qt::DirectConnection);
    connect(m_socket, SIGNAL(error(QAbstractSocket::SocketError)),
            this, SLOT(slotError(QAbstractSocket::SocketError)),
            Qt::DirectConnection);
}

void socket_thread::stop()
{
    m_socket->close();
    m_socket->deleteLater();
    emit finished();
}


void socket_thread::slotReadyRead()
{
    QDataStream in(m_socket);

    while(m_socket->bytesAvailable() > 0)
    {
        if(wait.wait_size == 0)
        {
            if(m_socket->bytesAvailable() < 8) break;
            in >> wait.wait_size;
        }
        if(m_socket->bytesAvailable() < wait.wait_size) break;
        if(wait.file_receive)
        {
            in.readRawData(data_data,wait.wait_size);
            wait.file.write(data_data,wait.wait_size);
            wait.file.waitForBytesWritten(1000);
            wait.file_received += wait.wait_size;
            qint64 now_waiting = wait.full_file_size - wait.file_received;
            if (now_waiting <=0 )
            {
                send_message("Файл " + wait.file.fileName() + " успешно принят, спасибо!", wait.name_transmitter);
                QFileInfo f(wait.file);
                QUrl a = QUrl::fromLocalFile(f.absoluteFilePath());
                QDesktopServices::openUrl(a);
                wait.resetReceive();
                emit file_percent_received(0);
                emit play_sound(QUrl("qrc:/sound/done"));
            }
            else
            {
                if (now_waiting>=BLOCK_FILE_SIZE) wait.wait_size = BLOCK_FILE_SIZE;
                else wait.wait_size = now_waiting;
                emit file_percent_received(qRound((wait.file_received*1.0/wait.full_file_size)*100.0));
            }
        }
        else
        {
            data_structure data(in);
            wait.wait_size = 0;

            if(data.getType() == "request")
            {
                if(data.getMess() == "NAME_REQUEST")
                {
                    data_structure to_server_data (QTime::currentTime(),"request",user_name,"NAME_FEEDBACK","server");
                    m_socket->write(to_server_data.getData());
                }
            }
            if(data.getType() == "info")
            {
                QList<QString> info_list;
                QString person;
                in>>person;
                in>>info_list;
                info_list.removeOne(user_name);
                emit info_list_changed(info_list,person);
                emit play_sound(QUrl("qrc:/sound/enter"));
                info_list.clear();
            }
            if(data.getType() == "message")
            {
                QString ls;
                if (data.getNameTo() != "all") ls = " (личное сообщение)";
                emit received_message(data.getTimeQstr() + " " + data.getNameFrom() + ": ",
                                      "    " + data.getMess() + ls);
                emit play_sound(QUrl("qrc:/sound/mess"));
            }
            if(data.getType() == "file")
            {
                emit received_message(data.getTimeQstr() + " " + data.getNameFrom() + " передает Вам файл: ",
                                      data.getMess() + ", размером: " + QString::number(data.getFileOnlySize()/1024.0/1024.0,'f',2) + " МБ");
                wait.startReceive(data.getMess(),data.getFileOnlySize(),data.getNameFrom());
                if(data.getFileOnlySize()>=BLOCK_FILE_SIZE) wait.wait_size = BLOCK_FILE_SIZE;
                else wait.wait_size = data.getFileOnlySize();
                emit file_percent_received(1);
                emit play_sound(QUrl("qrc:/sound/file"));
            }
        }
    }
}

void socket_thread::slotError(QAbstractSocket::SocketError)
{
    emit sended_message(m_socket->errorString());
    reconnect(host_name,user_name);
}

void socket_thread::reconnect(QString host, QString user)
{
    m_socket->close();
    m_socket->deleteLater();
    QThread::sleep(3);
    user_name = user;
    host_name = host;
    start();
}

void socket_thread::slotConnected()
{
    emit received_message(QTime::currentTime().toString("HH:mm")+ " " + "CLIENT" + ": ",
                user_name + ", добро пожаловать в чат!");
}

void socket_thread::slotDisconnected()
{
    emit received_message(QTime::currentTime().toString("HH:mm")+ " " + "CLIENT" + ": ",
                user_name + ", соединение с разорвано");
}

void socket_thread::send_message(QString mess, QString name_to)
{
    data_structure data(QTime::currentTime(), "message", user_name, mess, name_to);

    if(wait.file_transmit)
    {
        wait.transmit_buffer.append(data.getData());

        if(name_to == "all")
        {
            emit sended_message(data.getTimeQstr() + " " + data.getNameFrom() + ": ");
            emit sended_message("    " + data.getMess());
            emit sended_message("Передача сообщения для " + name_to + " отложена");
        }
        else
        {
            emit sended_message("Уведомление о приеме файла" + name_to + " отложено");
        }
    }
    else
    {
        m_socket->write(data.getData());
        m_socket->waitForBytesWritten();
        if(name_to == "all")
        {
            emit sended_message(data.getTimeQstr() + " " + data.getNameFrom() + ": ");
            emit sended_message("    " + data.getMess());
        }
        else
        {
            emit sended_message("Уведомление о приеме файла");
        }
    }
}

void socket_thread::send_file(QString file_name, QString client_name)
{
    if(file_name.isEmpty()) return;
    QString file_name_to_send;
    QFile file(file_name);
    if(file.open(QFile::ReadOnly))
    {
        QFileInfo info (file);
        file_name_to_send = info.fileName();

    }
    else return;
    emit file_percent_transmitted(1);
    data_structure data (QTime::currentTime(), "file", user_name, file_name_to_send, client_name, file.size());

    m_socket->write(data.getData());
    m_socket->waitForBytesWritten();

    wait.startTransmit();
    double file_size = static_cast<double>(file.size());
    if (file_size < 1.0) file_size = 1.0;
    qint64 downloaded = 0;

    char* file_data = new char[BLOCK_FILE_SIZE];
    while (!file.atEnd())
    {
        qint64 i = file.read(file_data,BLOCK_FILE_SIZE);
        m_socket->write(file_data,i);
        m_socket->waitForBytesWritten();
        downloaded += i;
        emit file_percent_transmitted(qRound((downloaded/file_size)*100.0));
    }
    delete [] file_data;

    emit file_percent_transmitted(0);
    file.close();
    if(!wait.transmit_buffer.isEmpty())
    {
        m_socket->write(wait.transmit_buffer);
        m_socket->waitForBytesWritten();
        wait.transmit_buffer.clear();
    }
    wait.resetTransmit();
}


