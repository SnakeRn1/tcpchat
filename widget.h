#ifndef WIDGET_H
#define WIDGET_H

#include <QWidget>
#include <QListWidgetItem>
#include <QGraphicsEffect>
#include <QMouseEvent>
#include <QScreen>
#include <QTimer>
#include <QDesktopWidget>
#include <QMediaPlayer>
#include "socket_thread.h"
#include <cmath>

QT_BEGIN_NAMESPACE
namespace Ui { class Widget; }
QT_END_NAMESPACE

class Widget : public QWidget
{
    Q_OBJECT

public:
    Widget(QWidget *parent = nullptr);
    ~Widget();

signals:
    void start_client();
    void stop_client();
    void send_message_sig(QString mess, QString name_to);
    void send_file_sig(QString file, QString name_to);
    void reconnect_sig(QString host,QString user);

private slots:
    void on_listWidget_itemClicked(QListWidgetItem *item);
    void slotSendToServer();
    void timerFileCheck();
    void on_button_exit_clicked();
    void on_edit_name_returnPressed();
    void on_edit_server_box_activated(int index);
    void on_check_autostart_stateChanged(int arg1);
    void on_check_sound_stateChanged(int arg1);

public slots:
    void append_received_message(QString,QString);
    void append_sended_message(QString);
    void progress_view_transmitted(int);
    void progress_view_received(int);
    void new_info_slot(QList<QString>,QString);
    void play_sound_slot(QUrl);

private:
    void mouseMoveEvent(QMouseEvent *event);
    void mousePressEvent(QMouseEvent *event);
    void closeEvent(QCloseEvent *event);
    bool eventFilter(QObject *watched, QEvent *event);
    void try_to_write_autostart_reg_val(bool on);
    QString get_host();

    Ui::Widget *ui;
    socket_thread *worker_socket;
    QThread* worker_thread;
    QGraphicsDropShadowEffect *shadowEffect;
    QGraphicsDropShadowEffect *element_shading;
    QPoint d;
    QMultiMap <QString,QString> file_names;
    QTimer tm;\
    QSettings* chat_settings;
    QMediaPlayer* sound;


};

#endif // WIDGET_H
