#include "widget.h"
#include "ui_widget.h"


Widget::Widget(QWidget *parent)
    : QWidget(parent)
    , ui(new Ui::Widget)
{
    ui->setupUi(this);
    worker_socket = nullptr;
    setWindowFlags(Qt::FramelessWindowHint);
    setAttribute(Qt::WidgetAttribute::WA_TranslucentBackground);

    shadowEffect = new QGraphicsDropShadowEffect(this);
    shadowEffect->setOffset(0);
    shadowEffect->setBlurRadius(15);
    shadowEffect->setColor(Qt::GlobalColor::yellow);
    ui->widget->setGraphicsEffect(shadowEffect);

    element_shading = new QGraphicsDropShadowEffect(this);
    element_shading->setOffset(0);
    element_shading->setBlurRadius(20);
    element_shading->setColor(Qt::GlobalColor::yellow);
    ui->m_ptxtInfo->setGraphicsEffect(element_shading);

    ui->m_ptxtInput->setFocus();
    ui->m_ptxtInput->installEventFilter(this);

    sound = new QMediaPlayer(this);

    connect(&tm, SIGNAL(timeout()), this, SLOT(timerFileCheck()));
    tm.start(1000);

    chat_settings = new QSettings ( QSettings::IniFormat,
                                    QSettings::UserScope,
                                    "SnakeR Soft",
                                    "Hola Chat",
                                    this);

    int serv_ind = chat_settings->value("server_index").toInt();
    if ((serv_ind<0)||(serv_ind>1)) serv_ind = 0;


    QString user = chat_settings->value("user_name").toString();
    if(!user.isEmpty())
    {
        ui->m_ptxtInput->setText(user);
        ui->edit_name->setText(user);
    }
    else ui->m_ptxtInfo->setText("Введите ваше имя..");

    ui->edit_server_box->setCurrentIndex(serv_ind);

    bool shared_settings_bool = false;

    if(!chat_settings->contains("autostart")) chat_settings->setValue("autostart",true);
    shared_settings_bool = chat_settings->value("autostart").toBool();

    ui->check_autostart->setChecked(shared_settings_bool);
    try_to_write_autostart_reg_val(shared_settings_bool);

    if(!chat_settings->contains("sound")) chat_settings->setValue("sound",true);
    shared_settings_bool = chat_settings->value("sound").toBool();
    ui->check_sound->setChecked(shared_settings_bool);

    slotSendToServer();
}

Widget::~Widget()
{
    emit stop_client();
    delete ui;
}

void Widget::slotSendToServer()
{
    if(ui->m_ptxtInput->toPlainText().isEmpty()) return;
    if(worker_socket == nullptr)
    {
        ui->m_ptxtInfo->setTextColor(QColor(Qt::GlobalColor::red));
        ui->m_ptxtInfo->append("Соединение...");
        ui->m_ptxtInfo->setAlignment(Qt::AlignCenter);
        worker_thread = new QThread;
        worker_socket = new socket_thread(get_host(), ui->m_ptxtInput->toPlainText());
        worker_socket->moveToThread(worker_thread);
        qRegisterMetaType<QList<QString>>();

        //По работе нити совместно с обработчиком
        connect(worker_thread,      SIGNAL  (started()),
                worker_socket,      SLOT    (start()),
                Qt::DirectConnection);
        connect(worker_socket,      SIGNAL  (finished()),
                worker_thread,      SLOT    (quit()),
                Qt::DirectConnection);
        connect(worker_socket,      SIGNAL  (finished()),
                worker_socket,      SLOT    (deleteLater()),
                Qt::DirectConnection);
        connect(worker_thread,      SIGNAL  (finished()),
                worker_thread,      SLOT    (deleteLater()),
                Qt::DirectConnection);

        //По приему сигналов обработчика
        connect(worker_socket,      SIGNAL  (file_percent_transmitted(int)),
                this,               SLOT    (progress_view_transmitted(int)),
                Qt::QueuedConnection);

        connect(worker_socket,      SIGNAL  (file_percent_received(int)),
                this,               SLOT    (progress_view_received(int)),
                Qt::QueuedConnection);

        connect(worker_socket,      SIGNAL  (received_message(QString,QString)),
                this,               SLOT    (append_received_message(QString,QString)),
                Qt::QueuedConnection);

        connect(worker_socket,      SIGNAL  (sended_message(QString)),
                this,               SLOT    (append_sended_message(QString)),
                Qt::QueuedConnection);

        connect(worker_socket,      SIGNAL  (info_list_changed (QList<QString>,QString)),
                this,               SLOT    (new_info_slot(QList<QString>,QString)),
                Qt::QueuedConnection);

        connect(worker_socket,      SIGNAL  (play_sound (QUrl)),
                this,               SLOT    (play_sound_slot(QUrl)),
                Qt::QueuedConnection);

        //По передаче сигналов обработчику
        connect(this,               SIGNAL  (send_file_sig(QString, QString)),
                worker_socket,      SLOT    (send_file(QString, QString)),
                Qt::QueuedConnection);

        connect(this,               SIGNAL  (send_message_sig(QString,QString)),
                worker_socket,      SLOT    (send_message(QString,QString)),
                Qt::QueuedConnection);

        connect(this,               SIGNAL  (stop_client()),
                worker_socket,      SLOT    (stop()),
                Qt::QueuedConnection);

        connect(this,               SIGNAL  (reconnect_sig(QString,QString)),
                worker_socket,      SLOT    (reconnect(QString,QString)),
                Qt::QueuedConnection);

        worker_thread->start();

        setWindowTitle(ui->m_ptxtInput->toPlainText() + " чат");
        chat_settings->setValue("user_name",ui->m_ptxtInput->toPlainText());
        ui->edit_name->setText(ui->m_ptxtInput->toPlainText());
        ui->m_ptxtInfo->clear();
        ui->m_ptxtInput->clear();

        return;
    }
    emit send_message_sig(ui->m_ptxtInput->toPlainText(),"all");
}

void Widget::timerFileCheck()
{
    if ((worker_socket != nullptr
         &&(!worker_socket->isTransmit())
         &&(!file_names.isEmpty())))
    {
        QString rx_name (file_names.firstKey());
        QString rx_file (file_names.take(rx_name));
        emit send_file_sig(rx_file,rx_name);
    }
}

void Widget::append_received_message(QString inf, QString txt)
{
    ui->m_ptxtInfo->setTextColor(QColor(Qt::GlobalColor::darkBlue));
    ui->m_ptxtInfo->append(inf);
    ui->m_ptxtInfo->setAlignment(Qt::AlignLeft);
    ui->m_ptxtInfo->setTextColor(QColor(Qt::GlobalColor::black));
    ui->m_ptxtInfo->append(txt);
    ui->m_ptxtInfo->setAlignment(Qt::AlignLeft);
}

void Widget::append_sended_message(QString txt)
{
    ui->m_ptxtInfo->setTextColor(QColor(Qt::GlobalColor::darkGreen));
    ui->m_ptxtInfo->append(txt);
    ui->m_ptxtInfo->setAlignment(Qt::AlignRight);
    ui->m_ptxtInput->clear();
}

void Widget::play_sound_slot(QUrl sound_url)
{
    if(ui->check_sound->isChecked())
    {
        sound->stop();
        sound->setMedia(sound_url);
        sound->play();
    }
}

void Widget::progress_view_transmitted(int val)
{
    if(val>0)
    {
        if(!ui->progressBar->isEnabled())
        {
            ui->progressBar->setEnabled(true);
            ui->progressBar->setFormat("Передано: %p%");
        }
        ui->progressBar->setValue(val);
    }
    else
    {
        ui->progressBar->setValue(0);
        ui->progressBar->setFormat("");
        ui->progressBar->setEnabled(false);
    }
}

void Widget::progress_view_received(int val)
{
    if(val>0)
    {
        if(!ui->progressBar->isEnabled())
        {
            ui->progressBar->setEnabled(true);
            ui->progressBar->setFormat("Принято: %p%");
        }
        ui->progressBar->setValue(val);
    }
    else
    {
        ui->progressBar->setValue(0);
        ui->progressBar->setFormat("");
        ui->progressBar->setEnabled(false);
    }
}

void Widget::new_info_slot(QList<QString> info_list, QString person)
{
    ui->m_ptxtInfo->setTextColor(QColor(Qt::GlobalColor::darkGray));
    ui->m_ptxtInfo->append("Участников чата: "+QString::number(info_list.size()+1));
    ui->m_ptxtInfo->setAlignment(Qt::AlignCenter);
    ui->m_ptxtInfo->append(person);
    ui->m_ptxtInfo->setAlignment(Qt::AlignCenter);
    ui->listWidget->clear();
    ui->listWidget->addItems(info_list);
}

void Widget::mouseMoveEvent(QMouseEvent *event)
{
    if((event->buttons() == Qt::LeftButton))
    {
        QPoint to_move = event->globalPos()-d;
        QPoint br = geometry().topLeft() - geometry().bottomRight() + QApplication::screenAt(event->globalPos())->availableGeometry().bottomRight();
        QPoint tl = QApplication::screenAt(event->globalPos())->availableGeometry().topLeft();;
        tl.setY(tl.y()+30);
        QPoint delta_br = to_move-br;
        QPoint delta_tl = to_move-tl;
        if(abs(delta_br.x()) <= 40) to_move.setX(br.x()+10+roundf(delta_br.x()*fabs(delta_br.x()/100.0f)));
        if(abs(delta_br.y()) <= 40) to_move.setY(br.y()+10+roundf(delta_br.y()*fabs(delta_br.y()/100.0f)));
        if(abs(delta_tl.x()) <= 40) to_move.setX(tl.x()-10+roundf(delta_tl.x()*fabs(delta_tl.x()/100.0f)));
        if(abs(delta_tl.y()) <= 40) to_move.setY(tl.y()-10+roundf(delta_tl.y()*fabs(delta_tl.y()/100.0f)));
        move(to_move);
    }
}

void Widget::mousePressEvent(QMouseEvent *event)
{
    d = event->pos();
    ui->m_ptxtInput->setFocus();
}

void Widget::closeEvent(QCloseEvent *event)
{
    emit stop_client();
    event->accept();
}

bool Widget::eventFilter(QObject *watched, QEvent *event)
{
    if(event->type() == QEvent::KeyPress)
    {
        if(watched == ui->m_ptxtInput)
        {
            QKeyEvent *keyEvent = (QKeyEvent *)event;
            if(keyEvent->key() == Qt::Key_Return)
            {
                slotSendToServer();
                return true;
            }
        }
    }
    return false;
}

void Widget::on_listWidget_itemClicked(QListWidgetItem *item)
{
    QStringList file_names_list = QFileDialog::getOpenFileNames();
    foreach(QString fileName,file_names_list)
    {
        file_names.insert(item->text(),fileName);
    }
    ui->toolBox->setCurrentIndex(0);
}

void Widget::on_button_exit_clicked()
{
    close();
}

void Widget::on_edit_name_returnPressed()
{
    if(worker_socket == nullptr) return;
    QString old_user = chat_settings->value("user_name").toString();
    QString new_user = ui->edit_name->text();
    if(new_user.isEmpty()) ui->edit_name->setText(old_user);
    if(old_user == new_user) return;
    if(ui->edit_name->text().isEmpty()) return;
    emit reconnect_sig(get_host(),ui->edit_name->text());
    setWindowTitle(ui->edit_name->text() + " чат");
    chat_settings->setValue("user_name",ui->edit_name->text());
    ui->toolBox->setCurrentIndex(0);
}

void Widget::on_edit_server_box_activated(int)
{
    if(worker_socket == nullptr) return;
    emit reconnect_sig(get_host(),ui->edit_name->text());
    ui->toolBox->setCurrentIndex(0);
}

QString Widget::get_host ()
{
    chat_settings->setValue("server_index",ui->edit_server_box->currentIndex());
    if(ui->edit_server_box->currentIndex() == 0) return "hola-git.ru";
    else return "hola-git.ru";
}

void Widget::on_check_autostart_stateChanged(int arg1)
{
    if(arg1 == 0) try_to_write_autostart_reg_val(false); // Это должно выключить, если включено
    else try_to_write_autostart_reg_val(true); // Это должно включить, если не включено.
}

void Widget::try_to_write_autostart_reg_val (bool on)
{
    chat_settings->setValue("autostart",on);
#ifdef Q_OS_WIN32
    QSettings settings("HKEY_CURRENT_USER\\SOFTWARE\\Microsoft\\Windows\\CurrentVersion\\Run", QSettings::NativeFormat);
    QString name("Hola Chat");
    QString app_file(QDir::toNativeSeparators(QCoreApplication::applicationFilePath()));
    bool cont = settings.contains(name);
    if(on&&!cont) settings.setValue(name,app_file); // Это должно включить, если не включено.
    if(!on&&cont) settings.remove(name); // Это должно выключить, если включено
#endif
}

void Widget::on_check_sound_stateChanged(int arg1)
{
    if(arg1 == 0)chat_settings->setValue("sound",false);
    else chat_settings->setValue("sound",true);
}
