QT += core network gui widgets
CONFIG += c++11
TEMPLATE = app
TARGET = ../hola_chat-dst/HolaChatServer
DEFINES += QT_DEPRECATED_WARNINGS
SOURCES += main_server.cpp mytcpserver.cpp data_structure.cpp
HEADERS += mytcpserver.h data_structure.h
VERSION = 1.1.3



win32 {
RC_ICONS += icons/serv_ico.ico
QMAKE_TARGET_COMPANY = "SnakeR Soft"
QMAKE_TARGET_PRODUCT = "HolaChatServer"
QMAKE_TARGET_DESCRIPTION = "HolaChatServer_x64"
QMAKE_TARGET_COPYRIGHT = "Copyright © 2022 Horuzhiy Ruslan"
}

qnx: target.path = /tmp/$${TARGET}/bin
else: unix:!android: target.path = /opt/$${TARGET}/bin
!isEmpty(target.path): INSTALLS += target
